<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="ar.com.libreria.modelo.TipoLibro"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <title>Formulario</title>

        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    </head>
    <body>

        <h1>Bienvenido</h1>
        <hr>

        <div class="container">
            <form action="LibroServlet" method="POST">

                <div class="form-group">
                    <label for="nombre"><span
                            class="glyphicon glyphicon-asterisk"></span> Nombre</label> <input
                        type="text" class="form-control" id="nombre" name="nombre"
                        placeholder="Ingresar Nombre">
                </div>

                <div class="form-group">
                    <label for="autor"><span class="glyphicon glyphicon-user"></span>
                        Autor</label> <input type="text" class="form-control" id="autor"
                                         name="autor" placeholder="Ingresar autor" maxlength="100">
                </div>

                <div class="form-group">
                    <label for="tipolibro"><span class="glyphicon glyphicon-time"></span>Tipo de Libro</label>
                    <select class="custom-select" id="tipolibro" name ="tipolibro">
                        <%
                            List<TipoLibro> tipoLibros = (ArrayList<TipoLibro>) request.getAttribute("tipoLibros");
                            for (TipoLibro tipoLibro : tipoLibros) {
                                String descripcion = tipoLibro.getDescripcion();
                                Integer id = tipoLibro.getId();
                        %>
                        <option value=<%=id%>><%=descripcion%></option>
                        <%
                            }
                        %>
                    </select>
                </div>

                <div class="form-group">
                    <label for="anio"><span class="glyphicon glyphicon-time"></span>
                        A�o</label> <input type="text" class="form-control" id="anio" name="anio"
                                       placeholder="Ingresar a�o del libro">
                </div>
                <div class="form-group">
                    <label for="ubicacion"><span
                            class="glyphicon glyphicon-list-alt"></span> Ubicacion</label> <input
                        type="text" class="form-control" id="ubicacion" name="ubicacion"
                        placeholder="Ingresar Ubicacion">
                </div>
                <input type="submit" value="Enviar" class="btn btn-success btn-block"
                       id="enviar">
            </form>



        </div>

    </body>
</html>