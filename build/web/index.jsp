<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <title>Ingrese sus datos</title>
        <link rel="stylesheet"
              href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    </head>
    <body>

        <div class="container">
            <h1>Bienvenido</h1>
            <hr>

            <h4>Ingrese datos:</h4>
            <BR>

            <%
                String resultado = request.getAttribute("info") == null ? "OK" : "NO";
                if (resultado.equalsIgnoreCase("NO")) {
            %>
            <div class="alert alert-danger" role="alert">Los datos
                ingresados son incorrectos!</div>
                <%
                    }
                %>

            <form action="LoginServlet" method="POST">

                <div class="form-group">
                    <label for="nombre"><span class="glyphicon glyphicon-user"></span>
                        Usuario</label> <input type="text" class="form-control" id="nombre"
                                           name="nombre" placeholder="Ingresar Nombre">
                </div>
                <div class="form-group">
                    <label for="autor"><span
                            class="glyphicon glyphicon-asterisk"></span> Password</label> <input
                        type="password" class="form-control" id="password" name="password"
                        placeholder="Ingresar autor" maxlength="100">
                </div>
                <input type="submit" value="Enviar" class="btn btn-success btn-block"
                       id="enviar">
            </form>
        </div>
</html>