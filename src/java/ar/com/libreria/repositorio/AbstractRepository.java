/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.libreria.repositorio;

import java.util.List;

/**
 *
 * @author EducaciónIT
 */
public abstract class AbstractRepository<T> {
    
    public abstract List<T> findAll();
    
    public abstract T findById(Integer id);
    
    public abstract T save(T entity);
}
