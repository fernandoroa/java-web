/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.libreria.repositorio;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author EducaciónIT
 */
public class AdministradorConexiones {

    public static Connection obtenerConexion() throws Exception {
        String dbDriver = "com.mysql.jdbc.Driver";

        String dbConnString = "jdbc:mysql://localhost/libreria?autoReconnect=true&useSSL=false";

        String dbUser = "root";

        String dbPassword = "root";

        Class.forName(dbDriver).newInstance();
        Connection connection = DriverManager.getConnection(dbConnString, dbUser, dbPassword);
        return connection;
    }

}
