/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.libreria.modelo;

/**
 *
 * @author EducaciónIT
 */
public class Libro {

    private Integer id;
    private String nombre;
    private String autor;
    private Integer id_tipo;
    private String anio;
    private String ubicacion;
    private String genero;

    public Libro() {
    }

    public Libro(Integer id, String nombre, String autor, Integer id_tipo, String anio, String ubicacion) {
        this.id = id;
        this.nombre = nombre;
        this.autor = autor;
        this.id_tipo = id_tipo;
        this.anio = anio;
        this.ubicacion = ubicacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Integer getId_tipo() {
        return id_tipo;
    }

    public void setId_tipo(Integer id_tipo) {
        this.id_tipo = id_tipo;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return "Nombre :" + getNombre() + " Ubicacion :" + getUbicacion();
    }

}
