/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.libreria.servlet.utils;

/**
 *
 * @author fernando.melendez
 */
public class ServletUtils {

    public static boolean isNumeric(String value) {
        boolean isValid = true;
        try {
            Double.valueOf(value);
        } catch (Exception ex) {
            isValid = false;
        }
        return isValid;
    }

}
