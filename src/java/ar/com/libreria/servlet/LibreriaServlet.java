package ar.com.libreria.servlet;

import ar.com.libreria.dao.LibroDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ar.com.libreria.dao.TipoLibroDao;
import ar.com.libreria.modelo.Libro;
import ar.com.libreria.modelo.TipoLibro;

/**
 * Servlet implementation class LibreriaServlet
 */
@WebServlet("/LibreriaServlet")
public class LibreriaServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public LibreriaServlet() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       PrintWriter out = response.getWriter();

        String nombreIdLibro = request.getParameter("nombreIdLibro");

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Datos de la Persona </title>");
        out.println("<link rel=\"stylesheet\"\r\n"
                + "	href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\">");
        out.println("</head>");
        out.println("<body>");
        out.println("<div class=\"container\">");
        out.println("<table style=\"width:100%\">");
        out.println("<tr>");
        out.println("<th>Nombre</th>");
        out.println("<th>Autor</th>");
        out.println("<th>Genero</th>");
        out.println("<th>Anio</th>");
        out.println("<th>Ubicacion</th>");
        out.println("</tr>");

        LibroDao libroDao = new LibroDao();
        List<Libro> libros = libroDao.findAll();
        for (Libro libro : libros) {
            out.println("<tr>");
            out.println("<td>" + libro.getNombre() + "</td>");
            out.println("<td>" + libro.getAutor() + "</td>");
            out.println("<td>" + libro.getGenero() + "</td>");
            out.println("<td>" + libro.getAnio() + "</td>");
            out.println("<td>" + libro.getUbicacion() + "</td>");
            out.println("</tr>");
        }

        out.println("</table>");
        out.println("</div>");
        out.println("</body>");
        out.println("</html>");
        out.close();

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        List<TipoLibro> tipoLibros = TipoLibroDao.getAllListTipoLibro();
        request.setAttribute("tipoLibros", tipoLibros);
        request.getRequestDispatcher("crear_libro.jsp").forward(request, response);
    }

}
