package ar.com.libreria.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ar.com.libreria.dao.LibroDao;
import ar.com.libreria.modelo.Libro;

/**
 * Servlet implementation class LibreriaServlet
 */
@WebServlet("/LibroServlet")
public class LibroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public LibroServlet() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");

		Libro libro = new Libro();
		libro.setNombre(request.getParameter("nombre"));
		libro.setAutor(request.getParameter("autor"));
		libro.setId_tipo(Integer.parseInt(request.getParameter("tipolibro")));
		libro.setAnio(request.getParameter("anio"));
		libro.setUbicacion(request.getParameter("ubicacion"));
		libro = new LibroDao().save(libro);

		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Datos de la Persona </title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<link rel=\"stylesheet\"\r\n"
				+ "	href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\">");
		out.println("</head>");
		out.println("<body>");
		out.println("<BR/>");
		out.println("<div class=\"container\">");

		if (libro.getId() != null) {
			out.println("<div class=\"alert alert-success\" role=\"alert\">");
			out.println("El libro ha sido almacenado!");
			out.println("</div>");
			out.println("<hr>");
			out.println("<p>Id: <b>" + libro.getId() + "</b></p>");
		} else {
			out.println("<div class=\"alert alert-danger\" role=\"alert\">");
			out.println("El libro no ha podido ser almacenado!");
			out.println("</div>");
			out.println("<hr>");
			out.println("<p>Id: <b>1</b></p>");
		}

		out.println("<p>El nombre es: <b>" + request.getParameter("nombre") + "</b></p>");
		out.println("<p>El autor es: <b>" + request.getParameter("autor") + "</b></p>");
		out.println("<p>El anio es: <b>" + request.getParameter("anio") + "</b></p>");
		out.println("<p>El ubicacion es: <b>" + request.getParameter("ubicacion") + "</b></p>");

		out.println("</div>");
		out.println("</body>");
		out.println("</html>");

		out.close();

	}

}
