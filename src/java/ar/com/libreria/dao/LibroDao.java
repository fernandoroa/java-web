/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.libreria.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import ar.com.libreria.modelo.Libro;
import ar.com.libreria.repositorio.AbstractRepository;
import ar.com.libreria.repositorio.AdministradorConexiones;

/**
 *
 * @author EducaciónIT
 */
public class LibroDao extends AbstractRepository<Libro> {

    @Override
    public List<Libro> findAll() {
        List<Libro> libros = new ArrayList<>();
        try {
            Connection conn = AdministradorConexiones.obtenerConexion();
            String sql = "SELECT * FROM libro";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            Map<Integer,String> mapTipoLibros = TipoLibroDao.getAllTipoLibro();
            
            while(rs.next()){
                Libro libro = new Libro();
                libro.setId(rs.getInt("id"));
                libro.setNombre(rs.getString("nombre"));
                libro.setAutor(rs.getString("autor"));
                libro.setId_tipo(rs.getInt("id_tipo"));
                libro.setAnio(rs.getString("anio"));
                libro.setUbicacion(rs.getString("ubicacion"));
                libro.setGenero(mapTipoLibros.get(rs.getInt("id_tipo")));
                libros.add(libro);
            }
            rs.close();
            st.close();
            conn.close();
        } catch (Exception ex) {
            System.err.println("ERRORRRRRR");
        }
        return libros;
    }

    @Override
    public Libro findById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Libro save(Libro entity) {
        try {

            StringBuffer sql = new StringBuffer();
            sql.append(
                    "INSERT INTO libro (id,nombre,autor,id_tipo,anio,ubicacion)");
            sql.append(" VALUES(?,?,?,?,?,?)");
            Connection con = AdministradorConexiones.obtenerConexion();
            PreparedStatement stmt = con.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, 0);
            stmt.setString(2, entity.getNombre());
            stmt.setString(3, entity.getAutor());
            stmt.setInt(4, entity.getId_tipo());
            stmt.setString(5, entity.getAnio());
            stmt.setString(6, entity.getUbicacion());
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            entity.setId(rs.getInt(1));

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(LibroDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(LibroDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return entity;
    }


}
