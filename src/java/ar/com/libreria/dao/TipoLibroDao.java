/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.libreria.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ar.com.libreria.modelo.TipoLibro;
import ar.com.libreria.repositorio.AdministradorConexiones;

/**
 *
 * @author EducaciónIT
 */
public class TipoLibroDao {

    public static Map<Integer, String> getAllTipoLibro() {
        Map<Integer, String> mapTipoLibro = new HashMap<>();
        try {
            Connection conn = AdministradorConexiones.obtenerConexion();
            String sql = "SELECT * FROM tipo_libro";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                mapTipoLibro.put(rs.getInt("id"), rs.getString("descripcion"));
            }
            rs.close();
            st.close();
            conn.close();
        } catch (Exception ex) {
            System.err.println("Falle al buscar los libros :O");
        }
        return mapTipoLibro;
    }
    
    
    public static List<TipoLibro> getAllListTipoLibro() {
    	List<TipoLibro> mapTipoLibro = new ArrayList<>();
        try {
            Connection conn = AdministradorConexiones.obtenerConexion();
            String sql = "SELECT * FROM tipo_libro GROUP BY id";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
            	mapTipoLibro.add(new TipoLibro(rs.getInt("id"), rs.getString("descripcion")));
            }
            rs.close();
            st.close();
            conn.close();
        } catch (Exception ex) {
            System.err.println("Falle al buscar los tipos de libros");
        }
        return mapTipoLibro;
    }


}
